from django.db import models
from django.contrib.auth.models import User


class Books(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    books = models.JSONField(null=True) #[models.IntegerField()]
    links = models.JSONField(null=True)
    owner = models.ForeignKey(User, related_name='books', on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
