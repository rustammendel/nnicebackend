from rest_framework import routers

from books.api import BooksViewSet

router = routers.DefaultRouter()
router.register('api/books', BooksViewSet, 'books')
urlpatterns = router.urls
