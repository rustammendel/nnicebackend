from rest_framework import viewsets, permissions, generics

from books.serializers import BooksSerializers


class BooksViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.IsAuthenticated
    ]
    serializer_class = BooksSerializers

    def get_queryset(self):
        return self.request.user.books.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


# Get Links API

class LinksAPI(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    serializer_class = BooksSerializers

    def get_object(self):
        return self.request.user
